define([
	"require",
	"dojo/store/JsonRest",
	"dojo/_base/config"
], function (
	require,
	JsonRest,
	config
) {
	return new JsonRest({
		target: config.pathPrefix + "/services/driver/",
		idProperty: "id",
		getLabel: function(item) {
			return item.name;
		}
	});
});
