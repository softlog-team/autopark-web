define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/query",
    "dojomat/_AppAware",
    "../_global/widget/NavigationWidget",
    "dojo/text!./templates/HomePage.html",
    "dojo/text!./css/HomePage.css"
], function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    query,
    _AppAware,
    NavigationWidget,
    template,
    css
) {
    return declare([_WidgetBase, _TemplatedMixin, _AppAware], { // << updated
        request: null,
        router: null,
        session: null,
        templateString: template,
        navigationWidget: null,

        constructor: function (params) {
            this.request = params.request;
            this.router = params.router;
            this.session = params.session;
        },

        postCreate: function () {
            this.inherited(arguments);
            this.setCss(css); // updated
            this.setTitle('Home lolka'); // updated

            this.navigationWidget = new NavigationWidget({
                router: this.router // << updated
            }, this.navigationNode);
        },

        startup: function () {
            this.inherited(arguments);
            this.navigationWidget.startup();
        }

        /*postCreate: function () {
            this.inherited(arguments);
            var navigationWidget = new NavigationWidget({}, this.navigationNode);

            // setting style
            var styleElement = window.document.createElement('style');
            styleElement.setAttribute("type", "text/css");
            query('head')[0].appendChild(styleElement);

            if (styleElement.styleSheet) {
                styleElement.styleSheet.cssText = css; // IE
            } else {
                styleElement.innerHTML = css; // the others
            }
        }*/
    });
});
