define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dijit/Toolbar",
    "dijit/form/DropDownButton",
    "dijit/DropDownMenu",
    "dijit/MenuItem",
    "dijit/TooltipDialog",
    "dijit/form/TextBox",
    "dojomat/_StateAware",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/text!./templates/NavigationWidget.html"
], function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    Toolbar,
    DropDownButton,
    DropDownMenu,
    MenuItem,
    TooltipDialog,
    TextBox,
    _StateAware,
    lang,
    on,
    template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
        router: null,
        templateString: template,

        constructor: function (params) {
            this.router = params.router;
        },

        postCreate: function () {
            this.inherited(arguments);

            this.own(on(this.logo, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("races").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuRaces, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("races").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuDailyReport, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("reportDaily").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));

            this.own(on(this.menuDevices, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("catalogDevices").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuCars, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("catalogCars").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuDrivers, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("catalogDrivers").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuPlaces, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("catalogPlaces").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
            this.own(on(this.menuRoutes, "click", lang.hitch(this, function (evt) {
                var url = this.router.getRoute("catalogRoutes").assemble();
                evt.preventDefault();
                this.pushState(url);
            })));
        }
    });
});
