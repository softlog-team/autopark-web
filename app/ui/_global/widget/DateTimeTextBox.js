define([
	"dojo/_base/declare",
	"dojo/_base/lang",
	"dojo/fx",
	"dojo/cldr/supplemental",
	"dojo/date",
	"dojo/date/locale",
	"dojo/dom",
	"dojo/dom-attr",
	"dojo/dom-class",
	"dojo/dom-geometry",
	"dijit/focus",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojo/text!./templates/DateTimeTextBox.html",
	"dijit/form/DateTextBox",
	"dijit/form/TimeTextBox",
	"dijit/form/Button"
], function(declare, lang, fx, cldr, date, locale, dom, domAttr, domClass, domGeom, focusUtil,
	_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template
){
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin ], {
		templateString : template, 
		target : "",
		time : new Date(),

		postCreate : function() {
			var controller = this;

			this.dateWidget.on("blur", lang.hitch(this, "_onBlur"));
			this.timeWidget.on("blur", lang.hitch(this, "_onBlur"));
		},

		focus: function() {
			focusUtil.focus(this.dateWidget);
		},

		_getValueAttr: function() {
			var value = new Date();
			value.setDate(this.dateWidget.getValue().getDate());
			value.setMonth(this.dateWidget.getValue().getMonth());
			value.setFullYear(this.dateWidget.getValue().getFullYear());
			value.setHours(this.timeWidget.getValue().getHours());
			value.setMinutes(this.timeWidget.getValue().getMinutes());
			value.setSeconds(this.timeWidget.getValue().getSeconds());
			return value.getTime();
		},

		_setValueAttr: function(datetime) {
			var curdate;
			if (datetime == null)
				curdate = new Date();
			else
				curdate = new Date(datetime);

			this.dateWidget.setValue(curdate);
			this.timeWidget.setValue(curdate);
		},

		_onBlur: function(e){
			var focused = focusUtil.activeStack[focusUtil.activeStack.length - 1];
			var elements = [ this.dateWidget.id, this.timeWidget.id ];
			if (this.disabled || elements.indexOf(focused) != -1) return;


			this.inherited(arguments);
		}
	});
});
