define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/DriversGrid",
	"../../service/car-store",
	"../../service/driverCategory-store",
	"../../service/driver-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/DriversPage.html",
	"dojo/text!./css/DriversPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	DriversGrid,
	carStore,
	driverCategoryStore,
	driverStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		driversGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Водители");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.driverStore = new Cache(Observable(driverStore),
					new Memory({ idProperty: "id" }));
			this.driverCategoryStore = new Cache(driverCategoryStore,
					new Memory({ idProperty: "id" }));
			this.carStore = new Cache(carStore,
					new Memory({ idProperty: "id" }));

			this.driversGrid = new DriversGrid({
				router : this.router,
				carStore : this.carStore,
				driverCategoryStore : this.driverCategoryStore,
				driverStore : this.driverStore
			}, this.driversNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu     = dijit.byId("menuCatalog");
				var menuItem = dijit.byId("menuDrivers");

				domClass.add(menu.domNode, "info");
				menu.set("label", menu.label + " &#10140; " + menuItem.label);
			});
		}
	});
});
