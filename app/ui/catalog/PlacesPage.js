define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/PlacesGrid",
	"../../service/place-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/PlacesPage.html",
	"dojo/text!./css/PlacesPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	PlacesGrid,
	placeStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		placesGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Места подачи");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.placeStore = new Cache(Observable(placeStore),
					new Memory({ idProperty: "id" }));

			this.placesGrid = new PlacesGrid({
				router     : this.router,
				placeStore : this.placeStore
			}, this.placesNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu     = dijit.byId("menuCatalog");
				var menuItem = dijit.byId("menuPlaces");

				domClass.add(menu.domNode, "info");
				menu.set("label", menu.label + " &#10140; " + menuItem.label);
			});
		}
	});
});
