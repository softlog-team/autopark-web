define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/DeviceTypesGrid",
	"./widget/DevicesGrid",
	"../../service/deviceType-store",
	"../../service/device-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/DevicesPage.html",
	"dojo/text!./css/DevicesPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	DeviceTypesGrid,
	DevicesGrid,
	deviceTypeStore,
	deviceStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		deviceTypesGrid : null,
		devicesGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Автопарк | Устройства");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);


			var store = new Cache(
					Observable(deviceStore),
					new Memory({ idProperty: "id" }));
			var typeStore = new Cache(
					Observable(deviceTypeStore),
					new Memory({ idProperty: "id" }));

			this.deviceGrid = new DevicesGrid({
				router : this.router,
				deviceStore : store,
				typeStore : typeStore
			}, this.devicesNode);

			this.deviceTypesGrid = new DeviceTypesGrid({
				router : this.router,
				deviceStore : store,
				typeStore : typeStore
			}, this.typesNode);
		},

		startup : function() {
			this.inherited(arguments);

			var menu     = dijit.byId("menuCatalog");
			var menuItem = dijit.byId("menuDevices");

			domClass.add(menu.domNode, "info");
			menu.set("label", menu.label + " &#10140; " + menuItem.label);

			this.deviceTypesGrid.startup();
			this.deviceGrid.startup();
		}
	});
});
