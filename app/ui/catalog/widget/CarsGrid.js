define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/NumberSpinner",
	"dijit/form/DateTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/CarsGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	NumberSpinner,
	DateTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		templateString: template,
		carWidget: null,

		constructor: function (params) {
			this.router              = params.router;
			this.carStore            = params.carStore;
			this.carTypeStore        = params.carTypeStore;
			this.deviceStore         = params.deviceStore;
			this.driverCategoryStore = params.driverCategoryStore;
			this.carFuelStore        = params.carFuelStore;
			this.carStateStore       = params.carStateStore;
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();
			var grid = this.gridWidget;

			//console.log(this.carStore.query());
			grid.set("store", this.carStore);

			// Валидация
			/*grid.on("dgrid-datachange", lang.hitch(this, function(event) {
				if (!this.validate(event.cell, event.value)) {
					event.preventDefault();
					setTimeout(function() { grid.edit(event.cell); }, 0);
				}
			}));*/
		},

		buildGrid: function () {
			var columns = {
				id: {
					field: "id",
					label: "№"
				},
				capacity: editor({
					field: "capacity",
					label: "Груз.",
					editorArgs: {
				        smallDelta: 0.1,
				        constraints: { min:0.1, max:100, places:1 }
					},
					autoSave: true,
					formatter: function(value) {
						if (!value) return "<center>&mdash;</center>";
						return value.toFixed(1);
					}
				}, NumberSpinner, "click"),
				inspectDate: editor({
					field: "inspectDate",
					label: "Пройдено ТО",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				}, DateTextBox, "click"),
				license: editor({
					field: "license",
					label: "Госномер",
					sortable: true,
					editorArgs: {
						maxLength: 12,
						regExp: "[\\d\\sа-яА-Я]+",
						invalidMessage: "Неправильный госномер"
					},
					autoSave: true
				}, ValidationTextBox, "click"),
				manufactured: editor({
					field: "manufactured",
					label: "Дата выпуска",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				}, DateTextBox, "click"),
				mileage: editor({
					field: "mileage",
					label: "Пробег",
					editorArgs: {
				        smallDelta: 1,
				        constraints: { min:1, max:9999999, places:0 }
					},
					autoSave: true
				}, NumberSpinner, "click"),
				model: editor({
					field: "model",
					label: "Модель",
					editorArgs: { maxLength: 50 },
					autoSave: true
				}, TextBox, "click"),
				purchased: editor({
					field: "purchased",
					label: "Дата покупки",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				}, DateTextBox),
				type: editor({
					field: "type",
					label: "Тип",
					editorArgs: { store: this.carTypeStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.type) return "&mdash;";
						if (value == item.type.id && item.type.name)
							return item.type.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.type ? item.type.id : 0;
					},
					set: function(item) {
						if (!item.type)
							return null;
						if (typeof item.type.id == "undefined")
							return { id: item.type };

						return { id: item.type.id };
					}
				}, Select, "click"),
				device: editor({
					field: "device",
					label: "Устройство",
					editorArgs: { store: this.deviceStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.device) return "&mdash;";
						if (value == item.device.id && item.device.code)
							return item.device.code;

						return this.editorArgs.store.get(value).code;
					},
					get: function(item) {
						return item.device ? item.device.id : 0;
					},
					set: function(item) {
						if (!item.device)
							return null;
						if (typeof item.device.id == "undefined")
							return { id: item.device };

						return { id: item.device.id };
					}
				}, Select, "click"),
				driverCategory: editor({
					field: "driverCategory",
					label: "Кат.",
					editorArgs: { store: this.driverCategoryStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.driverCategory) return "&mdash;";
						if (value == item.driverCategory.id && item.driverCategory.name)
							return item.driverCategory.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.driverCategory ? item.driverCategory.id : 0;
					},
					set: function(item) {
						if (!item.driverCategory)
							return null;
						if (typeof item.driverCategory.id == "undefined")
							return { id: item.driverCategory };

						return { id: item.driverCategory.id };
					}
				}, Select, "click"),
				fuel: editor({
					field: "fuel",
					label: "Топл.",
					editorArgs: { store: this.carFuelStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value) return "&mdash;";
						if (value == item.fuel.id && item.fuel.name)
							return item.fuel.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.fuel ? item.fuel.id : 0;
					},
					set: function(item) {
						if (!item.fuel)
							return null;
						if (typeof item.fuel.id == "undefined")
							return { id: item.fuel };

						return { id: item.fuel.id };
					}
				}, Select, "click"),
				state: editor({
					field: "state",
					label: "Статус",
					editorArgs: { store: this.carStateStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value) return "&mdash;";
						if (value == item.state.id && item.state.name)
							return item.state.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.state ? item.state.id : 0;
					},
					set: function(item) {
						if (!item.state)
							return null;
						if (typeof item.state.id == "undefined")
							return { id: item.state };

						return { id: item.state.id };
					}
				}, Select, "click")
			};

			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>Автомобили не заданы</span></p>"
			}, this.gridNode);
		},

		addCar: function() {
			var grid  = this.gridWidget;
			var store = this.carStore;

			dojo.when(store.add({ capacity : 1.0,
				                  model    : "Новый автомобиль",
				                  license  : "",
				                  mileage  : 0
				                }), function(result) {
				grid.refresh();
				/*result.model = "Автомобиль " + result.id;
				store.put(result).then(function() { grid.refresh() });*/
				on.once(grid, "dgrid-refresh-complete", function() {
					grid.select(result.id);

					var cell = grid.cell(result.id, "model");
					grid.edit(cell);

					query("input", cell.element)[0].select();
				});
			});
		},

		deleteCar: function() {
			var grid = this.gridWidget;
			var store = this.carStore;

			for (i in grid.selection) {
				if (!grid.selection[i]) continue;

				store.remove(i);
			}
			grid.refresh();
		},

		validate: function(cell, value) {
			var dialog = null;
			var message = null;

			switch (cell.column.id) {
				case "model":
					if (!value.length)
						message = "Модель не может быть пустой!";
					break;
			}

			if (message) {
				Tooltip.show(message, cell.element, [ "below" ]);
				setTimeout(function() {
					on.once(document, "keydown, click", function() {
						Tooltip.hide(cell.element);
					});
				}, 500); // Ждём полсекунды, пока покажется tooltip

				return false;
			}

			return true;
		}
    });
});
