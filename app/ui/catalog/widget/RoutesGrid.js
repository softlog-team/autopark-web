define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/NumberSpinner",
	"dijit/form/DateTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/RoutesGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	NumberSpinner,
	DateTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		templateString: template,

		constructor: function (params) {
			this.router    = params.router;
			this.routeStore = params.routeStore;
			this.router    = params.router;
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();
			var grid = this.gridWidget;

			grid.set("store", this.routeStore);

			// Валидация
			grid.on("dgrid-datachange", lang.hitch(this, function(event) {
				if (!this.validate(event.cell, event.value)) {
					event.preventDefault();
					setTimeout(function() { grid.edit(event.cell); }, 0);
				}
			}));
		},

		buildGrid: function () {
			var columns = {
				id: {
					field: "id",
					label: "№"
				},
				name: editor({
					field: "name",
					label: "Название маршрута",
					editorArgs: { maxLength: 50 },
					autoSave: true
				}, TextBox, "click"),
				edit: {
					label: "",
					renderCell : lang.hitch(this, function(item, data, cell) {
						var that = this;
						var btnDelete = new Button({
							label: "Зона",
							iconClass: "icon-globe",
							onClick : function() {
								var url = that.router.getRoute("editRoute")
									.assemble({ id: item.id });
								that.pushState(url);
							}
						}, cell.appendChild(document.createElement("div")));
						btnDelete._destroyOnRemove = true;

						return btnDelete;

					})
				}
			};

			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>Маршруты не заданы</span></p>"
			}, this.gridNode);
		},

		addRoute: function() {
			var grid  = this.gridWidget;
			var store = this.routeStore;

			dojo.when(store.add({ name: "Новый маршрут" }), function(result) {
				grid.refresh();
				on.once(grid, "dgrid-refresh-complete", function() {
					grid.select(result.id);

					var cell = grid.cell(result.id, "name");
					grid.edit(cell);

					query("input", cell.element)[0].select();
				});
			});
		},

		deleteRoute: function() {
			var grid  = this.gridWidget;
			var store = this.routeStore;

			for (i in grid.selection) {
				if (!grid.selection[i]) continue;

				store.remove(i);
			}
			grid.refresh();
		},

		validate: function(cell, value) {
			var dialog = null;
			var message = null;

			switch (cell.column.id) {
				case "name":
					if (!value.length)
						message = "Название маршрута не может быть пустым!";
					break;
			}

			if (message) {
				Tooltip.show(message, cell.element, [ "below" ]);
				setTimeout(function() {
					on.once(document, "keydown, click", function() {
						Tooltip.hide(cell.element);
					});
				}, 500); // Ждём полсекунды, пока покажется tooltip

				return false;
			}

			return true;
		}
    });
});
