define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dijit/layout/LayoutContainer",
	"dijit/layout/ContentPane",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dijit/form/Button",
	"dijit/form/TextBox",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/_base/config",
	"dojo/date/locale",	
	"dojo/text!./templates/DeviceTypesGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	LayoutContainer,
	ContentPane,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Button,
	TextBox,
	Tooltip,
	on,
	aspect,
	query,
	config,
	locale,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		typeStore: null,
		templateString: template,
		gridWidget: null,

		constructor: function (params) {
			this.router = params.router;
			this.typeStore = params.typeStore;
			this.deviceStore = params.deviceStore;
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();

			var validate = this.validate;
			var grid     = this.gridWidget;

			// Заполняем данными
			grid.set("store", this.typeStore);

			// Валидация
			grid.on("dgrid-datachange", lang.hitch(this, function(event) {
				if (!this.validate(event.cell, event.value)) {
					event.preventDefault();
					setTimeout(function() { grid.edit(event.cell); }, 0);
				}
			}));

			/*aspect.after(this.typeStore, "put", function(result) {
				dojo.when(result, function() {}, function(err) {
					console.log(err);
			    });
			    return result;
			});*/
		},

		buildGrid: function () {
			var columns = {
				id: {
					field: "id",
					label: "№",
					sortable: true,
					width: "50"
				},
				configTime: {
					field: "configTime",
					label: "Время конфигурирования",
					sortable: true,
					formatter: function(datum) {
						if (!datum) return "&mdash;";
						else return locale.format(new Date(datum), {
							selector: "datetime",
							formatLength: "medium"
						});
					}
				},
				name: editor({
					field: "name",
					label: "Название",
					editorArgs: {
						maxLength: 50
					},
					autoSave: true
				}, TextBox, "click")
			};
			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>Типы устройств не заданы</span></p>"
			}, this.gridNode);
		},

		addType: function() {
			var grid  = this.gridWidget;
			var store = this.typeStore;

			store.add({ name: "Новый тип" }).then(function(result) {
				store.query().then(function() {
					grid.refresh();
	
					on.once(grid, "dgrid-refresh-complete", function() {
						grid.select(result.id);

						var cell = grid.cell(result.id, "name");
						grid.edit(cell);
						query("input", cell.element)[0].select();
					});
				});
			});
		},

		deleteType: function() {
			var typeStore = this.typeStore;
			var grid = this.gridWidget;

			this.deviceStore.query().then(function(result) {
				for (i in grid.selection) {
					if (!grid.selection[i]) continue;

					var remove = true;
					for (j in result) {
						if (result[j].deviceType && (result[j].deviceType.id == i) &&
						    !confirm("Найдены устройства типа \"" + typeStore.get(i).name + "\".\n" +
						    		 "Удалить выбранный тип и все его устройства?")) {
							remove = false;
							break;
						}
					}
					if (remove)
						typeStore.remove(i);
				}
			});

			grid.refresh();
		},

		validate: function(cell, value) {
			var dialog = null;
			var message = null;

			switch (cell.column.id) {
				case "name":
					if (!value.length)
						message = "Название не может быть пустым!"
					break;
			}

			if (message) {
				Tooltip.show(message, cell.element, [ "below" ]);
				setTimeout(function() {
					on.once(document, "keydown, click", function() {
						Tooltip.hide(cell.element);
					});
				}, 500); // Ждём полсекунды, пока покажется tooltip

				return false;
			}

			return true;
		}
	});
});
