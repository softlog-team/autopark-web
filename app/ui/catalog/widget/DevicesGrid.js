define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/DevicesGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		deviceStore: null,
		typeStore: null,
		templateString: template,
		gridWidget: null,

		constructor: function (params) {
			this.router      = params.router;
			this.deviceStore = params.deviceStore;
			this.typeStore   = params.typeStore;
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();

			var grid = this.gridWidget;
			var deviceStore = this.deviceStore;

			// Перезагружаем при изменении типов
			this.typeStore.query().observe(function() {
				var selection = lang.clone(grid.selection);
				grid.refresh({ keepScrollPosition: true });
				grid.set("selection", selection);
			});
			// Инициализируем данными только после загрузки типов
			this.typeStore.query().then(function() {
				grid.set("store", deviceStore);
			});

			// Валидация
			grid.on("dgrid-datachange", lang.hitch(this, function(event) {
				if (!this.validate(event.cell, event.value)) {
					event.preventDefault();
					setTimeout(function() { grid.edit(event.cell); }, 0);
				}
			}));
		},

		buildGrid: function () {
			var columns = {
					id: {
						field: "id",
						label: "№",
						sortable: true
					},
					code: editor({
						field: "code",
						label: "IMEI",
						sortable: true,
						autoSave: true,
						editorArgs: {
							maxLength: 15,
							regExp: "\\d+",
							invalidMessage: "IMEI может содержать только цифры"
						}
					}, ValidationTextBox, "click"),
					lastConfig: {
						field: "lastConfig",
						label: "Обновление конфигурации",
						sortable: true,
						formatter: function(datum) {
							if (!datum) return "&mdash;";
							else return locale.format(new Date(datum), {
								selector: "datetime",
								formatLength: "medium"
							});
						}
					},
					type: editor({
						field: "deviceType",
						label: "Тип",
						sortable: true,
						editorArgs: { store: this.typeStore },
						formatter: function(value, item) {
							if (!value || !item.deviceType) return "&mdash;";

							if (value == item.deviceType.id && item.deviceType.name)
								return item.deviceType.name;

							return this.editorArgs.store.get(value).name;
						},
						get: function(item) {
							return item.deviceType ? item.deviceType.id : 0;
						},
						set: function(item) {
							if (!item.deviceType)
								return null;
							else if (typeof item.deviceType.id == "undefined")
								return { id: item.deviceType };

							return { id: item.deviceType.id };
						},
						autoSave: true
					}, Select, "click"),
					model: editor({
						field: "model",
						label: "Модель",
						autoSave: true
					}, TextBox, "click")
				};

			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>Устройства не заданы</span></p>"
			}, this.gridNode);
		},

		addDevice: function() {
			var grid  = this.gridWidget;
			var store = this.deviceStore;

			dojo.when(store.add({ code: 0, model: "Новая модель" }), function(result) {
				grid.refresh();
				on.once(grid, "dgrid-refresh-complete", function() {
					grid.select(result.id);

					var cell = grid.cell(result.id, "code");
					grid.edit(cell);

					query("input", cell.element)[1].select();
				});
			});
		},

		deleteDevice: function() {
			var store = this.deviceStore;
			var grid = this.gridWidget;

			for (i in grid.selection) {
				if (!grid.selection[i]) continue;

				store.remove(i);
			}

			grid.refresh();
		},

		validate: function(cell, value) {
			var dialog = null;
			var message = null;

			switch (cell.column.id) {
				case "model":
					if (!value.length)
						message = "Модель не может быть пустой!";
					break;

				case "code":
					if (!value.length)
						message = "IMEI не может быть пустым!";
					break;
			}

			if (message) {
				Tooltip.show(message, cell.element, [ "below" ]);
				setTimeout(function() {
					on.once(document, "keydown, click", function() {
						Tooltip.hide(cell.element);
					});
				}, 500); // Ждём полсекунды, пока покажется tooltip

				return false;
			}

			return true;
		}
    });
});
