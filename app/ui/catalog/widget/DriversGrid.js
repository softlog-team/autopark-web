define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/NumberSpinner",
	"dijit/form/DateTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/DriversGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	NumberSpinner,
	DateTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		templateString: template,

		constructor: function (params) {
			this.router              = params.router;
			this.carStore            = params.carStore;
			this.driverCategoryStore = params.driverCategoryStore;
			this.driverStore         = params.driverStore;
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();
			this.gridWidget.set("store", this.driverStore);
		},

		buildGrid: function () {
			var columns = {
				id: {
					field: "id",
					label: "№"
				},
				name: editor({
					field: "name",
					label: "Имя",
					editorArgs: { maxLength: 100 },
					autoSave: true
				}, TextBox, "click"),
				licenseSer: editor({
					field: "licenseSer",
					label: "Серия ВУ",
					editorArgs: { maxLength: 4 },
					autoSave: true
				}, TextBox, "click"),
				licenseNum: editor({
					field: "licenseNum",
					label: "Номер ВУ",
					editorArgs: { maxLength: 8 },
					autoSave: true
				}, TextBox, "click"),
				licenseDate: editor({
					field: "licenseDate",
					label: "Выдача ВУ",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				}, DateTextBox, "click"),
				category: editor({
					field: "category",
					label: "Кат.",
					editorArgs: { store: this.driverCategoryStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.category) return "&mdash;";

						if (value == item.category.id && item.category.name)
							return item.category.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.category ? item.category.id : 0;
					},
					set: function(item) {
						if (!item.category)
							return null;
						if (typeof item.category.id == "undefined")
							return { id: item.category };

						return { id: item.category.id };
					}
				}, Select, "click"),
				car: editor({
					field: "car",
					label: "Автомобиль",
					editorArgs: { store: this.carStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.car) return "&mdash;";

						var res;
						if (value == item.car.id && item.car.model)
							res = item.car;
						else
							res = this.editorArgs.store.get(value);

						return res.model +
						       ((res.license) ? " [" + res.license + "]" : "");
					},
					get: function(item) {
						return item.car ? item.car.id : 0;
					},
					set: function(item) {
						if (!item.car)
							return null;
						if (typeof item.car.id == "undefined")
							return { id: item.car };

						return { id: item.car.id };
					}
				}, Select, "click")
			};

			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>Водители не заданы</span></p>"
			}, this.gridNode);
		},

		addDriver: function() {
			var grid  = this.gridWidget;
			var store = this.driverStore;

			dojo.when(store.add({ licenseDate : "",
				                  licenseNum  : "",
				                  licenseSer  : ""
				                }), function(result) {
				grid.refresh();
				on.once(grid, "dgrid-refresh-complete", function() {
					grid.select(result.id);

					var cell = grid.cell(result.id, "employee");
					grid.edit(cell);

					query("input", cell.element)[0].select();
				});
			});
		},

		deleteDriver: function() {
			var grid = this.gridWidget;
			var store = this.driverStore;

			for (i in grid.selection) {
				if (!grid.selection[i]) continue;

				store.remove(i);
			}
			grid.refresh();
		}
    });
});
