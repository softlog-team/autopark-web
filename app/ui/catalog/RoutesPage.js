define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/RoutesGrid",
	"../../service/route-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/RoutesPage.html",
	"dojo/text!./css/RoutesPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	RoutesGrid,
	routeStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		routesGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Маршруты");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.routeStore = new Cache(Observable(routeStore),
					new Memory({ idProperty: "id" }));

			this.routesGrid = new RoutesGrid({
				router     : this.router,
				routeStore : this.routeStore,
				router     : this.router
			}, this.routesNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu     = dijit.byId("menuCatalog");
				var menuItem = dijit.byId("menuRoutes");

				domClass.add(menu.domNode, "info");
				menu.set("label", menu.label + " &#10140; " + menuItem.label);
			});
		}
	});
});
