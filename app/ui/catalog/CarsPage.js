define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/CarsGrid",
	"../../service/car-store",
	"../../service/carFuel-store",
	"../../service/carState-store",
	"../../service/carType-store",
	"../../service/driverCategory-store",
	"../../service/device-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/CarsPage.html",
	"dojo/text!./css/CarsPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	CarsGrid,
	carStore,
	carFuelStore,
	carStateStore,
	carTypeStore,
	driverCategoryStore,
	deviceStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		carsGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Автомобили");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.carStore = new Cache(Observable(carStore),
					new Memory({ idProperty: "id" }));
			this.carTypeStore = new Cache(carTypeStore,
					new Memory({ idProperty: "id" }));
			this.deviceStore = new Cache(deviceStore,
					new Memory({ idProperty: "id" }));
			this.driverCategoryStore = new Cache(driverCategoryStore,
					new Memory({ idProperty: "id" }));
			this.carFuelStore = new Cache(carFuelStore,
					new Memory({ idProperty: "id" }));
			this.carStateStore = new Cache(carStateStore,
					new Memory({ idProperty: "id" }));

			this.carsGrid = new CarsGrid({
				router : this.router,
				carStore : this.carStore,
				carTypeStore : this.carTypeStore,
				deviceStore : this.deviceStore,
				driverCategoryStore : this.driverCategoryStore,
				carFuelStore : this.carFuelStore,
				carStateStore : this.carStateStore
			}, this.carsNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu     = dijit.byId("menuCatalog");
				var menuItem = dijit.byId("menuCars");

				domClass.add(menu.domNode, "info");
				menu.set("label", menu.label + " &#10140; " + menuItem.label);
			});
		}
	});
});
