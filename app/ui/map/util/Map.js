define([
	"dojo/_base/declare",
	"dojo/_base/config",
	"dojo/parser",
	"dojo/on",
	"dojo/dom",
	"dojo/dom-geometry",
	"dojo/query",
	"dojo/_base/lang",
	"dojo/dom-style",
	"dojo/date/locale"
], function(
	declare,
	config,
	parser,
	on,
	dom,
	domGeom,
	query,
	lang,
	domStyle,
	locale
) {
	return {
		map: null,
		jsts: null,
		lastFt: null,
		onZoneChanged: null,
		track: [],
		controls: {},
		selectedPoint: null,
		calcDistance: false,
		distPoints: [],

		init : function(div, options) {
			OpenLayers.ImgPath = config.pathPrefix + "/resources/";

			this.map = new OpenLayers.Map(div, {
				controls: [ new OpenLayers.Control.Navigation() ],
		    	projection: new OpenLayers.Projection('EPSG:900913'),
		    	displayProjection: new OpenLayers.Projection('EPSG:4326')
			});
			this.map.addLayer(new OpenLayers.Layer.OSM("OSM"));

	        var mousePos = new OpenLayers.Control.MousePosition();
	        mousePos.displayProjection = new OpenLayers.Projection(this.map.displayProjection);
	        mousePos.formatOutput = function(lonLat) {
	        	if (this.selectedPoint != null) {
	        		if (this.selectedPoint.geometry == null) {
	        			this._removePopups();
	        		} else {
		        		var mapEl = dom.byId(div);
	
						var mapPos = domGeom.position(mapEl);
						var elPos = domGeom.position(this.selectedPoint.geometry.id);
						var mouse = mousePos.lastXy;
	
						elPos.x -= mapPos.x + mapEl.clientLeft;
						elPos.y -= mapPos.y + mapEl.clientTop;
	
						if (mouse.x < elPos.x || mouse.x > elPos.x + elPos.w ||
						    mouse.y < elPos.y || mouse.y > elPos.y + elPos.h) {
							this._removePopups();
						}
	        		}
	        	}

	        	if (options !== undefined && "onPositionChanged" in options)
	        		options.onPositionChanged(lonLat.lon, lonLat.lat);
	        }.bind(this);
	        this.map.addControl(mousePos);

			var limits = {
					minY: 35.1584269,
					minX: 45.0497396,
					maxY: 36.4914632,
					maxX: 45.3648275
				}
			var bounds = new OpenLayers.Bounds();
			bounds.extend(new OpenLayers.LonLat(limits.minY, limits.minX)
				.transform(this.map.displayProjection, this.map.projection));
			bounds.extend(new OpenLayers.LonLat(limits.maxY, limits.maxX)
				.transform(this.map.displayProjection, this.map.projection));
			this.map.zoomToExtent(bounds);

			var distance = new OpenLayers.Layer.Vector("distance");
			var points   = new OpenLayers.Layer.Vector("points");
			var lines    = new OpenLayers.Layer.Vector("lines");
			var polygons = new OpenLayers.Layer.Vector("polygons");
			this.map.addLayers([ polygons, lines, points, distance ]);

			polygons.setZIndex(800);
			lines.setZIndex(802);
			points.setZIndex(803);
			distance.setZIndex(801);

			var that = this;
	        selectPoints = new OpenLayers.Control.SelectFeature(
	        	points,
	        	{
	        		hover: true,
	        		clickFeature: this._pointClicked.bind(this),
	        		eventListeners: {
	        			featurehighlighted: function(e) { that._pointSelected(e) }
	        		}
	        	}
	        );
	        this.map.addControl(selectPoints);
	        selectPoints.activate();

			this.map.events.on({
				zoomend: lang.hitch(this, function () {
					this._drawPoints();
				})
			});

        	if (options !== undefined && "onDistanceCalculated" in options)
        		this.onDistanceCalculated = options.onDistanceCalculated;

			if (options !== undefined && "onZoneChanged" in options)
				this.onZoneChanged = options.onZoneChanged;

			if (options !== undefined && "controls" in options) {
				this.controls = options.controls;

				for (var i in this.controls) {
					var control = this.controls[i];

					switch (i) {
					case 'select':
						control.feature = new OpenLayers.Control.SelectFeature(
							polygons,
							{
								clickout: true, toggle: false,
								multiple: false, hover: false,
								toggleKey: "ctrlKey",   // ctrl key removes from selection
								multipleKey: "shiftKey" // shift key adds to selection
							}
						);
						break;

					case 'draw':
						control.feature = new OpenLayers.Control.DrawFeature(
							polygons,
							OpenLayers.Handler.Polygon,
							{
								featureAdded: lang.hitch(this, function(feature) {
									this._polygonAdded(feature);
								})
							}
						);
						break;

					case 'edit':
						control.feature = new OpenLayers.Control.ModifyFeature(
							polygons,
							{
								mode:
									OpenLayers.Control.ModifyFeature.RESHAPE
									| OpenLayers.Control.ModifyFeature.DRAG,
								onModificationStart: lang.hitch(this, function(feature) {
									this.lastFt = feature.clone();
								}),
								onModification: lang.hitch(this, function(feature) {
									this._polygonModified(feature);
								})
							}
						);
						break;
					}
				}
			}

			for (var i in this.controls) {
				var ft = this.controls[i];
				var that = this;

				!function outer(ft) {
					that.map.addControl(ft.feature);
					if ("button" in ft)
						on(ft.button, "click", lang.hitch(that, function() {
							that._toggleFeature(ft.feature);
						}));
					else
						ft.feature.activate();
			    }(ft);
			}

			this.jsts = new jsts.io.OpenLayersParser();
		},

		_toggleFeature: function(feature) {
			var polygons = this.map.getLayersByName('polygons')[0];

			var type = null;
			for (var i in this.controls) {
				var ft = this.controls[i];
				if (!("button" in ft)) continue;

				ft.feature.deactivate();
				polygons.setZIndex(800);
				if (ft.feature != feature)
					ft.button.set('checked', false);
				else {
					type = i;
					if (!ft.button.get('checked'))
						return;
				}
			}

			polygons.setZIndex(804);
			feature.activate();
			if (type == "edit") {
				if (polygons.selectedFeatures.length > 0)
					feature.selectFeature(polygons.selectedFeatures[0]);
			}
		},

		_polygonModified: function(feature) {
			if (!("edit" in this.controls)) return;

			var geom = this.jsts.read(feature.geometry);
			if (geom.isValid()) {
				this.lastFt = feature.clone();
				this._zoneChanged();
				return;
			}

			var polygons = this.map.getLayersByName('polygons')[0];

			this.controls.edit.feature.deactivate();
			polygons.removeFeatures([ feature ]);
			polygons.addFeatures([ this.lastFt ]);
			this.controls.edit.feature.activate();
			this.controls.edit.feature.selectFeature(this.lastFt);
		},

		_polygonAdded: function(feature) {
			var layer = this.map.getLayersByName('polygons')[0];

			var jsts_geomA = this.jsts.read(feature.geometry);

			var found = false;
			var valid = jsts_geomA.isValid();

			var fts = layer.features;
			if (fts.length == 1 && valid) {
				this._zoneChanged();
				return;
			}

			for (var i = 0; i < fts.length && valid; i++) {
				if (fts[i].geometry == feature.geometry)
					continue;

				var jsts_geomB = this.jsts.read(fts[i].geometry);

				if (jsts_geomA.coveredBy(jsts_geomB)) {
					found = true;

					jsts_result_geom = jsts_geomB.difference(jsts_geomA);
					jsts_geomA = jsts_result_geom;
					break;
				} else if (jsts_geomA.intersects(jsts_geomB)
						&& !jsts_geomA.touches(jsts_geomB)) {
					found = true;

					jsts_result_geom = jsts_geomA.union(jsts_geomB);
					jsts_geomA = jsts_result_geom;
				}
			}

			if (!found) {
				layer.removeFeatures([ feature ]);
				return;
			}

			var ol_geom = this.jsts.write(jsts_result_geom);
			var ol_ft = new OpenLayers.Feature.Vector(ol_geom);

			layer.removeAllFeatures();
			layer.addFeatures([ ol_ft ]);

			this._zoneChanged();
		},

		clearTrack : function () {
			var lines = this.map.getLayersByName('lines')[0];
			var points = this.map.getLayersByName('points')[0];
			lines.destroyFeatures();
			points.destroyFeatures();
			this.track = [];
		},

		clearZone : function () {
			var polygons = this.map.getLayersByName('polygons')[0];
			polygons.destroyFeatures();
		},

		toggleDistance : function() {
			this.calcDistance = !this.calcDistance;
			if (!this.calcDistance)
				this._clearDistance();
		},

		_removePopups : function() {
			while (this.map.popups.length) {
				var popup = this.map.popups[0];

		    	this.map.removePopup(popup);
		    	popup.destroy();
			}

			this.selectedPoint = null;
		},

		_pointClicked : function()
		{
			if (!this.calcDistance || this.selectedPoint == null) return;

			var layer = this.map.getLayersByName('distance')[0];
			if (this.distPoints.length > 1)
				this._clearDistance();

			var style = {
					strokeColor: '#008000', 
					strokeWidth: 1,

					fillColor: '#00ff00',
					fillOpacity: '0.4',

					pointRadius: 8
				};
			var geom = new OpenLayers.Geometry.Point(
					this.selectedPoint.geometry.x,
					this.selectedPoint.geometry.y
				);
			geom.trackId = this.selectedPoint.geometry.trackId;
			this.distPoints.push(geom);

			var point = new OpenLayers.Feature.Vector(geom, null, style);
			layer.addFeatures(point);

			if (this.distPoints.length == 2) {
				var pointA = this.distPoints[0];
				var pointB = this.distPoints[1];

				var points = [];
				var startId = Math.min(pointA.trackId, pointB.trackId);
				var endId = Math.max(pointA.trackId, pointB.trackId);

				for (var i = startId; i <= endId; i++) {
					point = new OpenLayers.Geometry.Point(this.track[i].lon, this.track[i].lat);
					point.transform(this.map.displayProjection, this.map.projection);
					points.push(point);
				}

				var line = new OpenLayers.Geometry.LineString(points);
				var lineStyle = {
					strokeColor: '#00ff00',
					strokeOpacity: '0.4',
					strokeWidth: 6
				};

				var lineFeature = new OpenLayers.Feature.Vector(line, null, lineStyle);
				layer.addFeatures([lineFeature]);

				if (typeof this.onDistanceCalculated !== undefined)
					this.onDistanceCalculated(line.getGeodesicLength(this.map.projection));
			}
		},

		_clearDistance : function() {
			this.map.getLayersByName('distance')[0].destroyFeatures();
			this.distPoints = [];
		},

		_pointSelected : function(event)
		{
			if (this.selectedPoint != null)
				this._removePopups();

			var point = event.feature.geometry;
			var transformed = new dojo.clone(point).transform(
				new OpenLayers.Projection("EPSG:900913"),
				new OpenLayers.Projection("EPSG:4326")
			);

			var track = this.track;
			var pointTime = new Date(point.time);
			var content =
				'<tr><th>Дата:</th><td><b>' + pointTime.toLocaleDateString() + '</b></td></tr>' +
				'<tr><th>Время:</th><td><b>' + pointTime.toLocaleTimeString() + '</b></td></tr>';
			if (point.speed == 0) {
				var startTime = 0;
				var stopTime = 0;
				var startI = -1;

				for (var i = 0; i < track.length; i++) {
					if (startI < 0 &&
					    Math.abs(transformed.x - track[i].lon) < 0.000001 &&
					    Math.abs(transformed.y - track[i].lat) < 0.000001) {
						startTime = track[i].time;
						stopTime = startTime;
						startI = i;
					} else if (startI >= 0 && track[i].speed > 0) {
						stopTime = track[i].time;
						break;
					}
				}

				var seconds = Math.abs(stopTime - startTime) / 1000;

				var numdays = Math.floor((seconds % 31536000) / 86400); 
				var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
				var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
				var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;

				var strtime = '';
				if (numdays > 0)    strtime += '<b>' + numdays + '</b> д. ';
				if (numhours > 0)   strtime += '<b>' + numhours + '</b> ч. ';
				if (numminutes > 0) strtime += '<b>' + numminutes + '</b> мин. ';
				if (numseconds > 0) strtime += '<b>' + numseconds + '</b> с. ';
				strtime = strtime.trim();

				if (strtime != '')
					content = '<tr><th>Остановка:</th><td>' + strtime + '</td></tr>' + content;
			} else if (point.speed != undefined)
				content = '<tr><th>Скорость:</th><td><b>' + Math.round(point.speed * 1.852)
					+ '</b> км/ч</td></tr>' + content;

			if (point.time == track[0].time)
				content = '<tr><td colspan="2"><center><b>Старт</b></center></td></tr>' + content;
			else if (point.time == track[track.length - 1].time)
				content = '<tr><td colspan="2"><center><b>Финиш</b></center></td></tr>' + content;

			var popup = new OpenLayers.Popup.FramedCloud(
				'',
				point.getBounds().getCenterLonLat(),
				new OpenLayers.Size(0, 0),
				'<table align="center">' + content + '</table>',
				null,
				true
			);
			popup.autoSize = true;

			this.selectedPoint = event.feature;
			this.map.addPopup(popup);

			on(query(".olPopup")[0], "click", this._pointClicked.bind(this));
		},

		_distanceCalculated : null,

		_drawPoints : function () {
			if (this.track.length == 0) return;

			var layer = this.map.getLayersByName('points')[0];
			var zoom  = this.map.getZoom();
			var track = this.track;

			layer.destroyFeatures();

			var dists = [
				1000000, 400000, 200000, 2000000, 100000, 100000, 40000, 40000,
				20000, 20000, 10000, 3000, 2000, 1000, 400, 100, 0, 0, 0
			];

			var points = [];
			var pointStyle = { 
				strokeColor: '#003EC5', 
				fillColor: '#003EC5',
				strokeWidth: 1,
				pointRadius: 4
			};

			var k = 0;
			for (var i = 0; i < track.length; i++) {
				var point = new OpenLayers.Geometry.Point(track[i].lon, track[i].lat);
				point.transform(this.map.displayProjection, this.map.projection);
				point.speed = track[i].speed;
				point.time = track[i].time;
				point.trackId = i;

				var style = lang.clone(pointStyle);

				if ((typeof track[i].speed !== undefined) && (track[i].speed == 0)) {
					style.fillColor = '#ffff00';
					style.strokeColor = '#000000';
					style.graphicName = 'square';
				} else {
					style.graphicName = 'triangle';
					style.rotation = track[i].dir;
				}
				if ((i == 0) || (i == track.length - 1)) {
					if (i == 0) {
						style.externalGraphic = '../../../resources/track-start.png';
						style.graphicWidth = 20;
						style.graphicHeight = 30;
						style.graphicXOffset = -10;
						style.graphicYOffset = -30;
						style.rotation = 0;
					} else {
						style.externalGraphic = '../../../resources/track-end.png';
						style.graphicWidth = 25;
						style.graphicHeight = 30;
						style.graphicXOffset = -2;
						style.graphicYOffset = -30;
						style.rotation = 0;
					}

				}

				var visiblePoint = new OpenLayers.Feature.Vector(point, null, style);
				if ((i > 0) && (i < track.length - 1)) {
					var inDist = false;
					for (var j = 0; j < this.distPoints.length && !inDist; j++)
						if (this.distPoints[j].trackId == i)
							inDist = true;

					if (point.distanceTo(points[k].geometry) > dists[zoom] || inDist) {
						points.push(visiblePoint);
						k++;
					}
				} else
					points.push(visiblePoint);
			}

			layer.addFeatures(points);
		},

		renderTrack: function (track) {
			if (track.length == 0) return;

			var layer = this.map.getLayersByName('lines')[0];
			var points = [];

			for (var i = 0; i < track.length; i++) {
				track[i].id = i;
				point = new OpenLayers.Geometry.Point(track[i].lon, track[i].lat);
				point.transform(this.map.displayProjection, this.map.projection);
				points.push(point);
			}

			var line = new OpenLayers.Geometry.LineString(points);
			var lineStyle = {
				strokeColor: '#003EC5', 
				strokeOpacity: 1.0,
				strokeWidth: 2
			};

			var lineFeature = new OpenLayers.Feature.Vector(line, null, lineStyle);
			layer.addFeatures([lineFeature]);

			this.track = track;
			this._drawPoints();
		},

		renderZone: function(polygon) {
			var layer = this.map.getLayersByName('polygons')[0];

			var geoJson = new OpenLayers.Format.GeoJSON();
			var feature = geoJson.read(JSON.stringify(polygon))[0];

			feature.geometry.transform(this.map.displayProjection,
			                           this.map.projection);
			layer.addFeatures([ feature ]);
		},

		zoomIn  : function() { this.map.zoomIn(); },
		zoomOut : function() { this.map.zoomOut(); },
		zoomFit : function() {
			var layer = this.map.getLayersByName('polygons')[0];
			this.map.zoomToExtent(layer.getDataExtent());
		},

		_zoneChanged: function() {
			var layer = this.map.getLayersByName('polygons')[0];
			if (layer.features.length < 1) return;

			var geoJson = new OpenLayers.Format.GeoJSON();
			var geometry = layer.features[0].geometry.clone();
			geometry = geoJson.write(geometry.transform(
					this.map.projection,
					this.map.displayProjection));

			this.onZoneChanged(geometry);
		}
	}
});
