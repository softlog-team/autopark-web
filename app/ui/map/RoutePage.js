define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dojo/on",
	"dojo/_base/lang",
	"dojo/dom-style",
	"dojo/date/locale",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dijit/form/ToggleButton",
	"dijit/form/Select",
	"dijit/form/CheckBox",
	"dijit/form/DateTextBox",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dijit/Tooltip",
	"../../service/route-store",
	"../../service/race-store",
	"../../service/track-store",
	"../_global/widget/NavigationWidget",
	"./util/Map",
	"dojo/text!./templates/RoutePage.html",
	"dojo/text!./css/RoutePage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	on,
	lang,
	domStyle,
	locale,
	BorderContainer,
	ContentPane,
	ToggleButton,
	Select,
	CheckBox,
	DateTextBox,
	Observable,
	Memory,
	Tooltip,
	routeStore,
	raceStore,
	trackStore,
	NavigationWidget,
	map,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		route: null,
		track: [],

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Редактирование маршрута");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			//---------------------
			/*var limits = {
					minY: 35.1584269,
					minX: 45.0497396,
					maxY: 36.4914632,
					maxX: 45.3648275
				}
			var bounds = new OpenLayers.Bounds();
			bounds.extend(new OpenLayers.LonLat(limits.minY, limits.minX)
				.transform(map.displayProjection, map.projection));
			bounds.extend(new OpenLayers.LonLat(limits.maxY, limits.maxX)
				.transform(map.displayProjection, map.projection));
			map.zoomToExtent(bounds);*/
			//---------------------


			var options = {
				onZoneChanged: lang.hitch(this, function(g) { this.save(g) }),
				onPositionChanged: function(lon, lat) {
					var node = document.getElementById("mapCoords");
					if (node)
						node.innerHTML = "(" + lon + "; " + lat + ")";
				},
				controls: {
					select: {},
					draw: { button: this.drawButton },
					edit: { button: this.editButton	}
				}
			};

			map.init('map', options);
			map.clearTrack();

			on(this.zoomInButton,  "click", function() { map.zoomIn(); });
			on(this.zoomOutButton, "click", function() { map.zoomOut(); });

			this.raceSelect.watch("value", lang.hitch(this, function() {
				var value = this.raceSelect.get("value");
				map.clearTrack();

				if (value) {
					domStyle.set("mapLoading", "display", "block");
					trackStore.get(value).then(
						function(data) {
							if (data.length > 0) {
								var points = [];
								var i = 0;
								while (i < data.length) {
									points.push(data[i]);
									if (!data[i].speed) {
										while (i < data.length && !data[i].speed)
											i++;
									} else
										i++;
								}
								map.renderTrack(points);
							}

							domStyle.set("mapLoading", "display", "none");
						}
					);
				}
			}));

			this.load();
		},

		updateRaces: function () {
			var memoryStore = Observable(new Memory({
				idProperty: "id",
				getLabel: function(item) {
					return "№" + item.id;// + ", устройство " + item.device.model;
				}
			}));

			var routeId = this.route.id;

			raceStore.query({
				"date": locale.format(this.dateBox.get("value"), {
					selector: "date",
					datePattern: "yyyy-MM-dd"
				})
			}).then(function(data) {
				var raceCount = 0;
				data.forEach(function(item) {
					if (item.device != null && item.closeTime != null
							&& item.route != null && item.route.id == routeId) {
						memoryStore.put(item);
						raceCount++;
					}
				});

				if (raceCount > 0) {
					domStyle.set("raceSelect", "display", "inline-table");
					domStyle.set("noRacesLabel", "display", "none");
				} else {
					domStyle.set("raceSelect", "display", "none");
					domStyle.set("noRacesLabel", "display", "inline");
					
				}
			});

			this.raceSelect.setStore(memoryStore);
		},

		load: function() {
			var loc = document.location.pathname;
			if (loc.slice(-1) == '/')
				loc = loc.substr(0, loc.length - 1);
			var routeId = loc.substr(loc.lastIndexOf('/') + 1);

			routeStore.get(routeId).then(lang.hitch(this, function(item) {
				this.route = item;

				this.dateBox.watch("value", lang.hitch(this, function() {
					this.updateRaces();
				}));
				this.dateBox.set("value", new Date());

				if (item.zone.polygon == null) return;
				map.renderZone(item.zone.polygon);
			}));
		},

		save: function(geometry) {
			this.route.zone = { "polygon": JSON.parse(geometry) }
			routeStore.put(this.route);
		},

		startup: function() {
			this.inherited(arguments);
		}
	});
});
