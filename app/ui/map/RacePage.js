define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dojo/on",
	"dojo/dom",
	"dojo/dom-style",
	"dojo/date/locale",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dijit/form/ToggleButton",
	"dijit/form/Select",
	"dijit/form/CheckBox",
	"dijit/form/DateTextBox",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"../../service/race-store",
	"../../service/track-store",
	"../_global/widget/NavigationWidget",
	"./util/Map",
	"dojo/text!./templates/RacePage.html",
	"dojo/text!./css/RacePage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	on,
	dom,
	domStyle,
	locale,
	BorderContainer,
	ContentPane,
	ToggleButton,
	Select,
	CheckBox,
	DateTextBox,
	Observable,
	Memory,
	raceStore,
	trackStore,
	NavigationWidget,
	map,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		track: [],

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Просмотр рейса");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			map.init("map", {
				onPositionChanged: function(lon, lat) {
					var node = document.getElementById("mapCoords");
					if (node)
						node.innerHTML = "(" + lon + "; " + lat + ")";
				},
				onDistanceCalculated: function(distance) {
					var node = document.getElementById("distance");
					if (node)
						node.innerHTML = "Расстояние: <b>" + (distance / 1000).toFixed(2) + "</b> км.";
				}
			});
			map.clearTrack();

			on(this.zoomInButton,  "click", function() { map.zoomIn(); });
			on(this.zoomOutButton, "click", function() { map.zoomOut(); });
			on(this.reloadButton, "click", this.loadRace.bind(this));
			on(this.distanceButton, "click", function() {
				if (!this.distanceButton.checked)
					dom.byId("distance").innerHTML = "";

				map.toggleDistance();
			}.bind(this));

			this.load();
		},

		loadRace: function(zoomFit) {
			map.clearZone();
			map.clearTrack();

			var value = this.raceSelect.get("value");
			if (value) {
				raceStore.get(value).then(function(item) {
					this.updateTrack(value);
					map.renderZone(item.zone.polygon);
					if (typeof zoomFit !== undefined && zoomFit === true)
						map.zoomFit();
				}.bind(this));
			}
		},

		updateTrack: function(raceId) {
			domStyle.set("mapLoading", "display", "block");
			trackStore.get(raceId).then(
				function(data) {
					if (data.length > 0)
						map.renderTrack(data);

					domStyle.set("mapLoading", "display", "none");
				}
			);
		},

		updateRaces: function () {
			var memoryStore = Observable(new Memory({
				idProperty: "id",
				getLabel: function(item) {
					return "№" + item.id;
				}
			}));

			this.raceSelect.setStore(memoryStore);

			return raceStore.query({
				"date": locale.format(this.dateBox.get("value"), {
					selector: "date",
					datePattern: "yyyy-MM-dd"
				})
			}).then(function(data) {
				var raceCount = 0;
				data.forEach(function(item) {
					if (item.device != null) {
						memoryStore.put(item);
						raceCount++;
					}
				});

				if (raceCount > 0) {
					domStyle.set("raceSelect", "display", "inline-table");
					domStyle.set("noRacesLabel", "display", "none");
				} else {
					domStyle.set("raceSelect", "display", "none");
					domStyle.set("noRacesLabel", "display", "inline");
				}
			});
		},

		load: function() {
			var loc = document.location.pathname;
			if (loc.slice(-1) == '/')
				loc = loc.substr(0, loc.length - 1);
			var raceId = loc.substr(loc.lastIndexOf('/') + 1);
			var self = this;

			raceStore.get(raceId).then(function(item) {
				self.race = item;

				self.dateBox.set("value", locale.parse(item.execDate, {
					datePattern: "yyyy-MM-dd",
					selector: "date"
				}));

				self.updateRaces().then(function() {
					self.raceSelect.set("value", raceId);
				}).then(function() {
					self.updateTrack(raceId);
					self.dateBox.watch("value", self.updateRaces.bind(self));
					self.raceSelect.watch("value", function() {
						self.loadRace(true);
					});
				});

				if (item.zone == null || item.zone.polygon == null)
					return;

				map.renderZone(item.zone.polygon);
				map.zoomFit();
			});
		},

		save: function(geometry) {
			this.route.zone = { "polygon": JSON.parse(geometry) }
			routeStore.put(this.route);
		},

		startup: function() {
			this.inherited(arguments);
		}
	});
});
