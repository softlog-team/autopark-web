define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/NumberSpinner",
	"dijit/form/DateTextBox",
	"../../_global/widget/DateTimeTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/keys",
    "dojo/number",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/DailyReportGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	NumberSpinner,
	DateTextBox,
	DateTimeTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
    keys,
    number,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		templateString: template,

		constructor: function (params) {
			this.router      = params.router;
			this.reportStore = params.reportStore;
		},

		onDateKeydown: function (event) {
			if (event.keyCode == keys.ENTER)
				this.updateGrid();
		},

		onDateWheel: function (event) {
			if (event.wheelDelta > 0)
				this.incDay();
			else
				this.decDay();
		},

		decDay: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "day", -1));
		},

		incDay: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "day", 1));
		},

		decMonth: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "month", -1));
		},

		incMonth: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "month", 1));
		},

		updateGrid: function () {
			var query = {
				"date": locale.format(this.date.get("value"), {
					selector: "date",
					datePattern: "yyyy-MM-dd"
				})
			};
			this.gridWidget.set("query", query);
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();

			this.date.set("value", new Date());
			this.updateGrid();

			var grid = this.gridWidget;
			grid.set("store", this.reportStore);
		},

		buildGrid: function () {
			var columns = {
				route: {
					field: "route",
					label: "Маршрут"
				},
				execDate: {
					field: "execDate",
					label: "Дата рейса",
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				},
				openTime: {
					field: "openTime",
					label: "Время открытия",
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							datePattern: "yyyy.MM.dd",
							timePattern: "HH:mm"
						});
					}
				},
				closeTime: {
					field: "closeTime",
					label: "Время закрытия",
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							datePattern: "yyyy.MM.dd",
							timePattern: "HH:mm"
						});
					}
				},
				driver: {
					field: "driver",
					label: "Водитель"
				},
				car: {
					field: "car",
					label: "Автомобиль"
				},
				mileage: {
					field: "mileage",
					label: "Пробег (км)",
					get: function(item) {
						if (!item.hasZone)
							item.mileageOutZone = -1;

						return item.mileage;
					},
					formatter: function(mileage) {
						return number.format(mileage, {
							places: 2
						});
					}
				},
				mileageOutZone: {
					field: "mileageOutZone",
					label: "Вне зоны (км)",
					formatter: function(mileage) {
						if (mileage < 0)
							return "<center>&mdash;</center>";

						return number.format(mileage, {
							places: 2
						});
					}
				}
			};

			return new (declare([OnDemandGrid, Selection, Keyboard, ColumnResizer]))({
				columns: columns,
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>За данный период рейсов не найдено</span></p>"
			}, this.gridNode);
		}
    });
});
