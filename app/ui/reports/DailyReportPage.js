define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/dom-class",
	"./widgets/DailyReportGrid",
	"../../service/dailyReport-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/DailyReportPage.html",
	"dojo/text!./css/DailyReportPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	domClass,
	DailyReportGrid,
	dailyReportStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		reportGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Отчёты");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.reportGrid = new DailyReportGrid({
				router      : this.router,
				reportStore : dailyReportStore
			}, this.reportNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu     = dijit.byId("menuReports");
				var menuItem = dijit.byId("menuDailyReport");

				domClass.add(menu.domNode, "info");
				menu.set("label", menu.label + " &#10140; " + menuItem.label);
			});
		}
	});
});
