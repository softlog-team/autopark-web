define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_AppAware",
	"dojo/parser",
	"dijit/layout/BorderContainer",
	"dijit/layout/ContentPane",
	"dojo/store/Cache",
	"dojo/store/Memory",
	"dojo/store/Observable",
	"dojo/dom-class",
	"./widget/RacesGrid",
	"../../service/race-store",
	"../../service/driver-store",
	"../../service/device-store",
	"../../service/car-store",
	"../../service/route-store",
	"../../service/place-store",
	"../_global/widget/NavigationWidget",
	"dojo/text!./templates/RacesPage.html",
	"dojo/text!./css/RacesPage.css"
], function(
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_AppAware,
	parser,
	BorderContainer,
	ContentPane,
	Cache,
	Memory,
	Observable,
	domClass,
	RacesGrid,
	raceStore,
	driverStore,
	deviceStore,
	carStore,
	routeStore,
	placeStore,
	NavigationWidget,
	template,
	css
) {
	return declare([ _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _AppAware ], {
		request : null,
		router : null,
		session : null,
		templateString : template,
		navigationWidget : null,
		racesGrid : null,

		constructor : function(params) {
			this.request = params.request;
			this.router  = params.router;
			this.session = params.session;
		},

		postCreate : function() {
			this.inherited(arguments);
			this.setCss(css);
			this.setTitle("Рейсы");

			this.navigationWidget = new NavigationWidget({
				router : this.router
			}, this.navigationNode);

			this.raceStore = new Cache(Observable(raceStore),
					new Memory({ idProperty: "id" }));
			this.driverStore = new Cache(driverStore,
					new Memory({ idProperty: "id" }));
			this.carStore = new Cache(carStore,
					new Memory({ idProperty: "id" }));
			this.deviceStore = new Cache(deviceStore,
					new Memory({ idProperty: "id" }));
			this.routeStore = new Cache(routeStore,
					new Memory({ idProperty: "id" }));
			this.placeStore = new Cache(placeStore,
					new Memory({ idProperty: "id" }));

			this.racesGrid = new RacesGrid({
				router      : this.router,
				raceStore   : this.raceStore,
				driverStore : this.driverStore,
				carStore    : this.carStore,
				deviceStore : this.deviceStore,
				routeStore   : this.routeStore,
				placeStore  : this.placeStore
			}, this.racesNode);
		},

		startup : function() {
			this.inherited(arguments);
			dojo.when(this.navigationWidget.startup(), function() {
				var menu = dijit.byId("menuRaces");

				domClass.add(menu.domNode, "info");
			});
		}
	});
});
