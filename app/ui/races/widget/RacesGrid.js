define([
	"dojo/_base/declare",
	"dijit/_WidgetBase",
	"dijit/_TemplatedMixin",
	"dijit/_WidgetsInTemplateMixin",
	"dojomat/_StateAware",
	"dojo/_base/lang",
	"dgrid/OnDemandGrid",
	"dgrid/Selection",
	"dgrid/Keyboard",
	"dgrid/editor",
	"dgrid/extensions/ColumnResizer",
	"dojo/store/Observable",
	"dojo/store/Memory",
	"dojo/store/Cache",
	"dijit/form/Button",
	"dijit/form/TextBox",
	"dijit/form/ValidationTextBox",
	"dijit/form/NumberSpinner",
	"dijit/form/DateTextBox",
	"../../_global/widget/DateTimeTextBox",
	"dijit/form/Select",
    "dijit/Tooltip",
    "dojo/on",
    "dojo/aspect",
    "dojo/query",
    "dojo/keys",
    "dojo/_base/config",
	"dojo/date/locale",
	"put-selector/put",
	"dojo/text!./templates/RacesGrid.html"
], function (
	declare,
	_WidgetBase,
	_TemplatedMixin,
	_WidgetsInTemplateMixin,
	_StateAware,
	lang,
	OnDemandGrid,
	Selection,
	Keyboard,
	editor,
	ColumnResizer,
	Observable,
	Memory,
	Cache,
	Button,
	TextBox,
	ValidationTextBox,
	NumberSpinner,
	DateTextBox,
	DateTimeTextBox,
	Select,
	Tooltip,
	on,
	aspect,
    query,
    keys,
	config,
	locale,
	put,
	template
) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _StateAware], {
		router: null,
		templateString: template,

		constructor: function (params) {
			this.router      = params.router;
			this.raceStore   = params.raceStore;
			this.driverStore = params.driverStore;
			this.carStore    = params.carStore;
			this.deviceStore = params.deviceStore;
			this.routeStore  = params.routeStore;
			this.placeStore  = params.placeStore;
		},

		onDateKeydown: function (event) {
			if (event.keyCode == keys.ENTER)
				this.updateGrid();
		},

		onDateWheel: function (event) {
			if (event.wheelDelta > 0)
				this.incDay();
			else
				this.decDay();
		},

		decDay: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "day", -1));
		},

		incDay: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "day", 1));
		},

		decMonth: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "month", -1));
		},

		incMonth: function () {
			var old = this.date.get("value");
			this.date.set("value", dojo.date.add(old, "month", 1));
		},

		updateGrid: function () {
			var query = {
				"date": locale.format(this.date.get("value"), {
					selector: "date",
					datePattern: "yyyy-MM-dd"
				})
			};
			this.gridWidget.set("query", query);
		},

		postCreate: function () {
			this.inherited(arguments);

			this.gridWidget = this.buildGrid();

			this.date.set("value", new Date());
			this.updateGrid();

			var grid = this.gridWidget;
			grid.set("store", this.raceStore);
		},

		buildGrid: function () {
			var columns = {
				id: {
					field: "id",
					label: "№"
				},
				route: editor({
					field: "route",
					label: "Маршрут",
					editorArgs: { store: this.routeStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.route)
							return "<center>&mdash;</center>";
						if (value == item.route.id && item.route.name)
							return item.route.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.route ? item.route.id : 0;
					},
					set: function(item) {
						if (!item.route)
							return null;
						if (typeof item.route.id == "undefined")
							return { id: item.route };

						return { id: item.route.id };
					}
				}, Select, "click"),
				execDate: editor({
					field: "execDate",
					label: "Дата рейса",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							selector: "date",
							formatLength: "medium"
						});
					}
				}, DateTextBox, "click"),
				taskTxt: editor({
					field: "task",
					label: "Задание",
					editorArgs: { maxLength: 2000 },
					autoSave: true
				}, TextBox, "click"),
				place: editor({
					field: "place",
					label: "Место подачи",
					editorArgs: { store: this.placeStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.place)
							return "<center>&mdash;</center>";
						if (value == item.place.id && item.place.name)
							return item.place.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.place ? item.place.id : 0;
					},
					set: function(item) {
						if (!item.place)
							return null;
						if (typeof item.place.id == "undefined")
							return { id: item.place };

						return { id: item.place.id };
					}
				}, Select, "click"),
				driver: editor({
					field: "driver",
					label: "Водитель",
					editorArgs: { store: this.driverStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.driver)
							return "<center>&mdash;</center>";
						if (value == item.driver.id && item.driver.name)
							return item.driver.name;

						return this.editorArgs.store.get(value).name;
					},
					get: function(item) {
						return item.driver ? item.driver.id : 0;
					},
					set: function(item) {
						if (!item.driver)
							return null;
						if (typeof item.driver.id == "undefined")
							return { id: item.driver };

						return { id: item.driver.id };
					}
				}, Select, "click"),
				car: editor({
					field: "car",
					label: "Автомобиль",
					editorArgs: { store: this.carStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.car)
							return "<center>&mdash;</center>";

						var res;
						if (value == item.car.id && item.car.model)
							res = item.car;
						else
							res = this.editorArgs.store.get(value);

						return res.model +
						       ((res.license) ? " [" + res.license + "]" : "");
					},
					get: function(item) {
						return item.car ? item.car.id : 0;
					},
					set: function(item) {
						if (!item.car)
							return null;
						if (typeof item.car.id == "undefined")
							return { id: item.car };

						return { id: item.car.id };
					}
				}, Select, "click"),
				device: editor({
					field: "device",
					label: "Устройство",
					editorArgs: { store: this.deviceStore },
					autoSave: true,
					formatter: function(value, item) {
						if (!value || !item.device)
							return "<center>&mdash;</center>";
						if (value == item.device.id && item.device.code)
							return item.device.code;

						return this.editorArgs.store.get(value).code;
					},
					get: function(item) {
						return item.device ? item.device.id : 0;
					},
					set: function(item) {
						if (!item.device)
							return null;
						if (typeof item.device.id == "undefined")
							return { id: item.device };

						return { id: item.device.id };
					}
				}, Select, "click"),
				openTime: editor({
					field: "openTime",
					label: "Время открытия",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							datePattern: "yyyy.MM.dd",
							timePattern: "HH:mm"
						});
					}
				}, DateTimeTextBox, "click"),
				closeTime: editor({
					field: "closeTime",
					label: "Время закрытия",
					autoSave: true,
					formatter: function(datum) {
						if (!datum) return "<center>&mdash;</center>";
						else return locale.format(new Date(datum), {
							datePattern: "yyyy.MM.dd",
							timePattern: "HH:mm"
						});
					}
				}, DateTimeTextBox, "click"),
				corr: editor({
					field: "corr",
					label: "Корректировка",
					editorArgs: {
				        smallDelta: 0.5,
				        constraints: { min:0.0, max:10000, places:1 }
					},
					autoSave: true,
					formatter: function(value) {
						if (!value) return "<center>&mdash;</center>";
						return value.toFixed(1);
					}
				}, NumberSpinner, "click"),
				corrCause: editor({
					field: "corrCause",
					label: "Причина корр.",
					editorArgs: { maxLength: 500 },
					autoSave: true
				}, TextBox, "click"),
				map: {
					label: "",
					renderCell : lang.hitch(this, function(item, data, cell) {
						var that = this;
						var btnDelete = new Button({
							label: "Карта",
							iconClass: "icon-globe",
							onClick : function() {
								var url = that.router.getRoute("viewRace")
									.assemble({ id: item.id });
								that.pushState(url);
							}
						}, cell.appendChild(document.createElement("div")));
						btnDelete._destroyOnRemove = true;

						return btnDelete;

					})
				}
			};

			return new (declare([OnDemandGrid, Keyboard, Selection, ColumnResizer]))({
				columns: columns,
				getBeforePut: false,
				sort: [{ attribute: "id" }],
				loadingMessage: config.loadingMsg,
				noDataMessage:
					"<p style='padding: 20px'>" +
					"<span class='alert alert-info'>За данный период рейсов не найдено</span></p>"
			}, this.gridNode);
		},

		addRace: function() {
			var grid  = this.gridWidget;
			var store = this.raceStore;

			dojo.when(store.add({}), lang.hitch(this, function(result) {
				var execDate = new Date(result.execDate);
				if (execDate != this.date.get("value")) {
					this.date.set("value", execDate);
					this.updateGrid();
				} else
					grid.refresh();

				on.once(grid, "dgrid-refresh-complete", function() {
					grid.select(result.id);

					var cell = grid.cell(result.id, "route");
					grid.edit(cell);

					query("input", cell.element)[0].select();
				});
			}));
		},

		deleteRace: function() {
			var grid  = this.gridWidget;
			var store = this.raceStore;

			for (i in grid.selection) {
				if (!grid.selection[i]) continue;

				store.remove(i);
			}
		},

		validate: function(cell, value) {
			var dialog = null;
			var message = null;

			switch (cell.column.id) {
				/*case "name":
					if (!value.length)
						message = "Название места подачи не может быть пустым!";
					break;*/
			}

			if (message) {
				Tooltip.show(message, cell.element, [ "below" ]);
				setTimeout(function() {
					on.once(document, "keydown, click", function() {
						Tooltip.hide(cell.element);
					});
				}, 500); // Ждём полсекунды, пока покажется tooltip

				return false;
			}

			return true;
		}
    });
});
