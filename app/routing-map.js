define(["dojo/_base/config", "require"], function (config, require) {
	var p = config.pathPrefix;

	return {
		races: {
			schema: p,
			widget: require.toAbsMid('./ui/races/RacesPage')
		},
		reportDaily: {
			schema: p + '/reports/daily',
			widget: require.toAbsMid('./ui/reports/DailyReportPage')
		},
		catalogDrivers: {
			schema: p + '/catalog/drivers/',
			widget: require.toAbsMid('./ui/catalog/DriversPage')
		},
		catalogDevices: {
			schema: p + '/catalog/devices/',
			widget: require.toAbsMid('./ui/catalog/DevicesPage')
		},
		catalogCars: {
			schema: p + '/catalog/cars/',
			widget: require.toAbsMid('./ui/catalog/CarsPage')
		},
		catalogPlaces: {
			schema: p + '/catalog/places/',
			widget: require.toAbsMid('./ui/catalog/PlacesPage')
		},
		catalogRoutes: {
			schema: p + '/catalog/routes/',
			widget: require.toAbsMid('./ui/catalog/RoutesPage')
		},
		editRoute: {
			schema: p + '/map/route/:id/',
			widget: require.toAbsMid('./ui/map/RoutePage')
		},
		viewRace: {
			schema: p + '/map/race/:id/',
			widget: require.toAbsMid('./ui/map/RacePage')
		}
	};
});
