var profile = (function(){
	return {
	    basePath: ".",
	    releaseDir: "resources",
	    action: "release",
	    cssOptimize: "comments",
	    mini: true,
	    optimize: "closure",
	    layerOptimize: "closure",
	    stripConsole: "none", // all|none
	    selectorEngine: "acme",

	    staticHasFeatures: {
	        // The trace & log APIs are used for debugging the loader, so we don’t need them in the build
	        'dojo-trace-api':0,
	        'dojo-log-api':0,

	        // This causes normally private loader data to be exposed for debugging, so we don’t need that either
	        'dojo-publish-privates':0,

	        // We’re fully async, so get rid of the legacy loader
	        'dojo-sync-loader':0,
	        
	        // dojo-xhr-factory relies on dojo-sync-loader
	        'dojo-xhr-factory':0,

	        // We aren’t loading tests in production
	        'dojo-test-sniff':0
	    },

        packages: [
            { name: "app",          location: "app"               },
            { name: "dojo",         location: "libs/dojo"         },
            { name: "dijit",        location: "libs/dijit"        },
            { name: "routed",       location: "libs/routed"       },
            { name: "dojomat",      location: "libs/dojomat"      },
            { name: "dgrid",        location: "libs/dgrid"        },
            { name: "xstyle",       location: "libs/xstyle"       },
            { name: "put-selector", location: "libs/put-selector" },
            { name: 'dbootstrap',   location: "libs/dbootstrap"   }
        ]
	};
})();
