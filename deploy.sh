#!/bin/sh

# build.sh from Dojo buildsripts
build=libs/util/buildscripts/build.sh

# Show only errors and warnings from stderr
# - First save stdout as &3 (&1 is duped into 3).
# - Next send stdout to stderr (&2 is duped into 1).
# - Send stderr to &3 (stdout) (&3 is duped into 2).
# - Close &3 (&- is duped into 3)
$build --profile autopark.profile.js 3>&1 1>&2 2>&3 3>&- | grep "ERROR\|WARNING"

# If deploy location specified, copy files there
if [ ! -z "$1" ]; then
    if [ ! -d "$1" ]; then
        echo "Can't deploy, «$1» is not a directory."
        exit 1;
    fi;

    cp -r resources "$1"
    cp index.html "$1"

    if [ $? -ne 0 ]; then
        echo "Error while deploying to «$1»"
        exit 1;
    fi;

    echo "Successfully deployed to «$1»"
fi;
